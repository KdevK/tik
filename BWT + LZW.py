class BWT:
    def encode(self, msg):
        msg = msg + "$"

        table = [msg[i:] + msg[:i] for i in range(len(msg))]
        # print('table = ', table)

        table = sorted(table)
        # print('sorted table = ', table)

        last_column = [row[-1:] for row in table]
        bwt = ''.join(last_column)

        return bwt

    def decode(self, bwt):
        table = [""] * len(bwt)

        for i in range(len(bwt)):
            table = [bwt[i] + table[i] for i in range(len(bwt))]
            # print('unsorted = ', table)
            table = sorted(table)
            # print('sorted    =', table)

        inverse_bwt = [row for row in table if row.endswith("$")][0]

        inverse_bwt = inverse_bwt.rstrip("$")

        return inverse_bwt


class LZW:
    def compress(self, uncompressed):
        dict_size = 256
        dictionary = {chr(i): chr(i) for i in range(dict_size)}

        w = ""
        result = []
        for c in uncompressed:
            wc = w + c
            if wc in dictionary:
                w = wc
            else:
                result.append(dictionary[w])
                dictionary[wc] = dict_size
                dict_size += 1
                w = c

        if w:
            result.append(dictionary[w])
        return result

    def decompress(self, compressed):
        dict_size = 256
        dictionary = {chr(i): chr(i) for i in range(dict_size)}

        w = result = compressed.pop(0)
        for k in compressed:
            if k in dictionary:
                entry = dictionary[k]
            elif k == dict_size:
                entry = w + w[0]
            else:
                raise ValueError('Bad compressed k: %s' % k)
            result += entry

            dictionary[dict_size] = w + entry[0]
            dict_size += 1

            w = entry
        return result


def bit_to_char(seq: str):
    ans = ''
    for i in seq.split():
        ans += chr(int(i, 2))
    return ans


# msg = 'ABACABA'
# print(msg)
#
# bwt_helper = BWT()
#
# code = bwt_helper.encode(msg)
# print(code)
#
# lzw_helper = LZW()
#
# compressed = lzw_helper.compress(code)
# print(compressed)
# decompressed = lzw_helper.decompress(compressed)
# print(decompressed)
#
# print(bwt_helper.decode(decompressed))
if __name__ == '__main__':
    with open('input_lzw.txt') as file:
        msg = file.read()
        print(msg)
        msg = bit_to_char(msg)
        print(msg)

        bwt_helper = BWT()

        code = bwt_helper.encode(msg)
        print(code)

        lzw_helper = LZW()

        compressed = lzw_helper.compress(code)
        print(compressed)
        decompressed = lzw_helper.decompress(compressed)
        print(decompressed)

        print(bwt_helper.decode(decompressed))

